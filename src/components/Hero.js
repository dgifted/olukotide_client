import classes from './Hero.module.css';
import ButtonRounded from "./ButtonRounded";

const Hero = () => {
    const onButtonClickHandler = (event) => {
        console.log('Button click event => ', event);
    };

    return (
        <div className={classes['app-hero']}>
            <h2>Find your professional tutor for your purpose</h2>
            <p>Olukotide is a learning exchange platform providing access to limitless resources,
                teaching and learning across the world. It is your one stop shop for ease and empowerment
                to our target audience.</p>
            <div>
                <ButtonRounded className={classes['button-color']}
                               onClickHandler={onButtonClickHandler}>Get started</ButtonRounded>
            </div>
        </div>
    );
};

export default Hero;
