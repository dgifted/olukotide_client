import useWindowSize from "../utils/useWindowSize";
import Carousel from "react-bootstrap/Carousel";
import Slide1Image from '../assets/images/img_2-sm.png';
import Slide2Image from '../assets/images/img_15-sm.png';
import Slide3Image from '../assets/images/img_17-sm.png';
import Slide4Image from '../assets/images/img_16-sm.png';
import classes from './Index.module.css';
import SearchBar from "../components/SearchBar";
import Card from "react-bootstrap/Card";
import features from "../utils/Features";
import Step1Image from '../assets/images/img_1-sm.png';
import Step2Image from '../assets/images/img_13-sm.png';
import Step3Image from '../assets/images/img_14-sm.png';
import InteractiveImage from '../assets/images/conversation.svg';
import FastResultImage from '../assets/images/Icon feather-check-circle.svg';
import FlexibleImage from '../assets/images/Path 9.svg';
import CapImage from '../assets/images/img_22-sm.png';
import Hero from "../components/Hero";
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Slider from 'react-slick';
import {featuredTutors} from "../utils/FeaturedTutorSlides";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faStar} from "@fortawesome/free-solid-svg-icons";
import {faHeart} from "@fortawesome/free-solid-svg-icons/faHeart";
import LoginModal from "../components/modals/LoginModal";

// const sliderConfig = {
//     dots: true,
//     slidesToShow: 4,
//     slidesToScroll: 1,
//     autoPlay: true,
//     autoplaySpeed: 3000
// };

const Index = () => {
    const {width, height} = useWindowSize();

    const initFeaturedTutorSlide = () => {
        let slidesToShow = 1;

        if (width < 1440) {
            slidesToShow = 3;
        } else if (width >= 1440) {
            slidesToShow = 4;
        }

        return slidesToShow;
    };

    return (
        <section>
            <section className={classes['app-carousel-container']}>
                <Carousel>
                    <Carousel.Item>
                        <img src={Slide1Image}
                             alt={'Carousel slide'}
                             className={classes['app-carousel-container__image']}/>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img src={Slide2Image}
                             alt={'Carousel slide'}
                             className={classes['app-carousel-container__image']}/>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img src={Slide3Image}
                             alt={'Carousel slide'}
                             className={classes['app-carousel-container__image']}/>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img src={Slide4Image}
                             alt={'Carousel slide'}
                             className={classes['app-carousel-container__image']}/>
                    </Carousel.Item>
                </Carousel>
                <Hero/>
                <SearchBar/>
            </section>
            <section className={`${classes['group-wrapper']} ${classes['featured-tutor']}`}>
                <h2 className={'mb-5'}>Featured tutors</h2>
                <div className={classes['featured-tutor-slide-container']}>
                    <Slider dots={false}
                            slidesToShow={initFeaturedTutorSlide()}
                            slidesToScroll={1}
                            autoPlay={true}
                            autoplaySpeed={3000}>
                        {featuredTutors.map((tutor, index) => (
                            <Card className={'featured-tutor-card'} key={index}>
                                <Card.Img variant={'top'} src={tutor.avatar}/>
                                <Card.Body>
                                    <h3 className={'featured-tutor-card__title mb-1'}>{tutor.name}</h3>
                                    <div className={'featured-tutor-card__qualification'}>{tutor.qualification}</div>

                                    <div style={{fontSize: '0.9rem'}} className={'my-3'}>
                                        <FontAwesomeIcon icon={faStar} className={'text-warning mr-1'}/>
                                        <span style={{fontWeight: 'bold'}}>{tutor.rating}</span>
                                    </div>

                                    <div className={'featured-tutor-card__bio mb-3'}>{tutor.bio}</div>

                                    <hr/>
                                    <div className={'d-flex justify-content-between'}>
                                        <FontAwesomeIcon icon={faHeart} className={'text-muted'}/>
                                        <span
                                            style={{fontSize: '0.9rem'}}>Ask Price: <b>{tutor.askPrice}</b> per hour</span>
                                    </div>
                                </Card.Body>
                            </Card>
                        ))}
                    </Slider>
                </div>
            </section>
            <section className={`container-fluid ${classes['group-wrapper']}`}>
                <div className={classes['action-box']}>
                    <h3>Find what you want to learn</h3>
                    <p>Browse all our categories and get a tutor for your preferred purpose.</p>
                    <div>
                        <button type={'button'}>Get started</button>
                    </div>
                </div>
                <div className={classes['features-wrapper']}>
                    {features.map((feature, index) => (
                        <Card className={classes['feature-card']} key={index}>
                            <Card.Img variant={'top'} src={feature.image} alt={'feature'}/>
                            <Card.Body className={'text-center'}>
                                <Card.Text>{feature.title}</Card.Text>
                            </Card.Body>
                        </Card>
                    ))}
                </div>
            </section>
            <section className={`container-fluid ${classes['group-wrapper']} ${classes['featured-topics']}`}>
                <h2 className={'text-center'}>Featured topics by categories</h2>
                <div className={'row my-5 px-5'}>
                    <div className={`col-12 col-sm-4 col-md-3 text-left ${classes['subject-group']}`}>
                        <h3>Languages</h3>
                        <div>
                            <h4>French</h4>
                            <span>28 tutors</span>
                        </div>
                        <div>
                            <h4>Yoruba</h4>
                            <span>50 tutors</span>
                        </div>
                        <div>
                            <h4>Igbo</h4>
                            <span>70 tutors</span>
                        </div>
                        <div>
                            <h4>Spanish</h4>
                            <span>10 tutors</span>
                        </div>
                    </div>

                    <div className={`col-12 col-sm-4 col-md-3 text-left ${classes['subject-group']}`}>
                        <h3>EYFS & Junior School</h3>
                        <div>
                            <h4>Pre - Kindergarten Maths</h4>
                            <span>90 tutors</span>
                        </div>
                        <div>
                            <h4>Kindergarten Language Arts</h4>
                            <span>70 tutors</span>
                        </div>
                        <div>
                            <h4>Fourth Grade Socials Studies</h4>
                            <span>40 tutors</span>
                        </div>
                        <div>
                            <h4>Third Grade Science</h4>
                            <span>100 tutors</span>
                        </div>
                    </div>

                    <div className={`col-12 col-sm-4 col-md-3 text-left ${classes['subject-group']}`}>
                        <h3>Middle age & High school</h3>
                        <div>
                            <h4>Ninth Grade Algebra</h4>
                            <span>50 tutors</span>
                        </div>
                        <div>
                            <h4>Fifth Grade Mathematics</h4>
                            <span>30 tutors</span>
                        </div>
                        <div>
                            <h4>Twelfth Grade Calculus</h4>
                            <span>70 tutors</span>
                        </div>
                        <div>
                            <h4>Sixth Grade Socials Studies</h4>
                            <span>20 tutors</span>
                        </div>
                    </div>

                    <div className={`col-12 col-sm-4 col-md-3 text-left ${classes['subject-group']}`}>
                        <h3>Professional Exam</h3>
                        <div>
                            <h4>IELTS</h4>
                            <span>10 tutors</span>
                        </div>
                        <div>
                            <h4>GRE Analytical writing</h4>
                            <span>5 tutors</span>
                        </div>
                        <div>
                            <h4>GRE Analytical writing</h4>
                            <span>30 tutors</span>
                        </div>
                        <div>
                            <h4>ICSAN</h4>
                            <span>10 tutors</span>
                        </div>
                    </div>
                </div>
            </section>
            <section className={`container-fluid ${classes['steps']}`}>
                <h3>How Olukotide works</h3>
                <div className={classes['steps-item-group']}>
                    <div className={classes['intro-text']}>
                        <h3>01. <br/> Tell us where you need our service</h3>
                        <p>With our list of categories, we’ll get the best tutor for you.</p>
                    </div>
                    <div>
                        <img src={Step1Image} alt={'step 1'}/>
                    </div>
                </div>
                <div className={`${classes['steps-item-group']}`}>
                    <div className={classes['alignment-correction']}>
                        <img src={Step2Image} alt={'step 2'}/>
                    </div>
                    <div className={classes['intro-text']}>
                        <h3>02. <br/> Book your Teacher</h3>
                        <p>Professional and experienced teachers are ready to teach you and work with your
                            qualifications.</p>
                    </div>
                </div>
                <div className={classes['steps-item-group']}>
                    <div className={classes['intro-text']}>
                        <h3>03. <br/>Meet your Tutor</h3>
                        <p>Take a time suits you, meet your tutor and start your lessons.</p>
                    </div>
                    <div>
                        <img src={Step3Image} alt={'step 3'}/>
                    </div>
                </div>
            </section>
            <section className={`container-fluid ${classes['group-wrapper']} ${classes['reasons']}`}>
                <h3 className={'mb-5'}>Why Olukotide is ideal for you</h3>
                <div className={`pt-5 ${classes['reasons-group']}`}>
                    <div className={classes['reasons-group-item']}>
                        <img src={InteractiveImage} alt={'Reason interactive'}/>
                        <h4>Interactive</h4>
                        <p>Learn topics via real-life illustration to grasp more while learning.</p>
                    </div>

                    <div className={classes['reasons-group-item']}>
                        <img src={FastResultImage} alt={'Reason fast result'}/>
                        <h4>Fast results</h4>
                        <p>Our professional teachers cover the essential topic with customized lesson plans to get you
                            results.</p>
                    </div>

                    <div className={classes['reasons-group-item']}>
                        <img src={FlexibleImage} alt={'Reason flexible'}/>
                        <h4>Flexible</h4>
                        <p>Take a time suits you, meet your tutor and start your lessons.</p>
                    </div>
                </div>
            </section>
            <section className={`container-fluid ${classes['group-wrapper']}`}>
                <div className={`row  ${classes['final']}`}>
                    <div className={`col-md-5 offset-md-6`}>
                        <h3>Become a tutor</h3>
                        <p>Olukotide provides an avenue for experienced teachers to manage and grow their
                            tutoring business. . We provide the tools and skills to teach what you love.</p>
                        <div className={'mt-5'}>
                            <button className={classes['start-action-button']}>Start today</button>
                        </div>
                    </div>
                </div>
                <div className={classes.cap}>
                    <img src={CapImage}/>
                </div>
            </section>
        </section>
    );
};

export default Index;
