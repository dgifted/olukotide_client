import EYFSImage from '../assets/images/img_3-sm.png';
import MiddleHighSchoolImage from '../assets/images/img_4-sm.png';
import ProfessionalExamImage from '../assets/images/img_5-sm.png'
import AdultLearningImage from '../assets/images/img_6-sm.png';
import UndergraduatesImage from '../assets/images/img_7-sm.png';
import WaecImage from '../assets/images/img_8-sm.png';
import ProgrammingImage from '../assets/images/img_9-sm.png';
import DesignImage from '../assets/images/img_10-sm.png';
import ActivitiesImage from '../assets/images/img_11-sm.png';
import RoboticsImage from '../assets/images/img_12-sm.png';

const features = [
    { image: EYFSImage, title: 'EYFS & Junior' },
    { image: MiddleHighSchoolImage, title: 'Middle age & High school' },
    { image: ProfessionalExamImage, title: 'Professional Exam' },
    { image: AdultLearningImage, title: 'Adult Learning' },
    { image: UndergraduatesImage, title: 'Undergraduates' },
    { image: WaecImage, title: 'WAEC & JAMB' },
    { image: ProgrammingImage, title: 'Programming' },
    { image: DesignImage, title: 'Design' },
    { image: ActivitiesImage, title: 'Extracurricular Activities' },
    { image: RoboticsImage, title: 'Robotics' },
];

export default features;
