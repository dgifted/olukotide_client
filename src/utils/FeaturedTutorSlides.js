import Card from "react-bootstrap/Card";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faStar} from "@fortawesome/free-solid-svg-icons";

import JohnShola from '../assets/images/img_18-sm.png';
import OpeyemiOkunla from '../assets/images/img_19-sm.png';
import LoraSprinfied from '../assets/images/img_20-sm.png';
import JoyGrace from '../assets/images/img_21-sm.png';
import Finesse from '../assets/images/img_23-sm.png';
import {faHeart} from "@fortawesome/free-solid-svg-icons/faHeart";
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const featuredTutors = [
    {
        name: 'John Shola',
        qualification: 'Masters in Languages & Arts',
        avatar: JohnShola,
        rating: 5,
        askPrice: 2000,
        bio: 'I will speak all major languages in Nigeria'
    },
    {
        name: 'Opeyemi Okunla',
        qualification: 'Certified tutor in IELTS',
        avatar: OpeyemiOkunla,
        rating: 4.9,
        askPrice: 5000,
        bio: 'I’ll tutor you to pass your IELTS exams and other professi...'
    },
    {
        name: 'Lora Sprinfied',
        qualification: 'Foreign Language Tutor',
        avatar: LoraSprinfied,
        rating: 5.0,
        askPrice: 5000,
        bio: 'Want to learn some foreign languages? Hit me up. I am ...'
    },
    {
        name: 'Joy Grace',
        qualification: 'Junior School Teacher',
        avatar: JoyGrace,
        rating: 4.8,
        askPrice: 1500,
        bio: 'Want to learn some foreign languages? Hit me up. I am ...'
    },
    {
        name: 'Finesse',
        qualification: 'Expert in React.js',
        avatar: Finesse,
        rating: 5.0,
        askPrice: 3000,
        bio: 'Let’s get start on your journey to becoming a Developer.'
    },
];

const featuredTutorsSlide = () => {
    return (
        <>
            {/*{featuredTutors.map((tutor, index) => (*/}
            {/*    <Card className={'featured-tutor-card'} key={index}>*/}
            {/*        <Card.Img variant={'top'} src={tutor.avatar}/>*/}
            {/*        <Card.Body>*/}
            {/*            <h3 className={'featured-tutor-card__title mb-1'}>{tutor.name}</h3>*/}
            {/*            <div className={'featured-tutor-card__qualification'}>{tutor.qualification}</div>*/}

            {/*            <div style={{fontSize: '0.9rem'}} className={'my-3'}>*/}
            {/*                <FontAwesomeIcon icon={faStar} className={'text-warning mr-1'}/>*/}
            {/*                <span style={{fontWeight: 'bold'}}>{tutor.rating}</span>*/}
            {/*            </div>*/}

            {/*            <div className={'featured-tutor-card__bio mb-3'}>{tutor.bio}</div>*/}

            {/*            <hr/>*/}
            {/*            <div className={'d-flex justify-content-between'}>*/}
            {/*                <FontAwesomeIcon icon={faHeart}/>*/}
            {/*                <span style={{fontSize: '0.9rem'}}>Ask Price: <b>{tutor.askPrice}</b> per hour</span>*/}
            {/*            </div>*/}
            {/*        </Card.Body>*/}
            {/*    </Card>*/}
            {/*))}*/}
        </>
    );
};

export {featuredTutors, featuredTutorsSlide};
