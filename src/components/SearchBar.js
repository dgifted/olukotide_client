import classes from './SearchBar.module.css';

const SearchBar = () => {
    return (
        <div className={classes['search-bar-container']}>
            <div className={classes['search-bar-fields']}>
                <div>

                </div>
                <input type={'text'} />
                <span className={classes['search-bar-fields__separator']}>&nbsp;</span>
                <select>
                    <option value="languages">Languages</option>
                    <option value="eyfs">EYFS & Junior</option>
                    <option value="middleAge">Middle age & High School</option>
                    <option value="professional">Professional Exam</option>
                </select>
            </div>
            <button className={`${classes['search-button']} ml-3`}>Search</button>
        </div>
    );
};

export default SearchBar;
