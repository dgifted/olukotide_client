import classes from './Navbar.module.css';
import Logo from '../assets/images/logo-sm.png';
import {NavLink} from "react-router-dom";
import ButtonRounded from "./ButtonRounded";
import Dropdown from "react-bootstrap/Dropdown";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faGlobe} from "@fortawesome/free-solid-svg-icons";

const Navbar = (props) => {
    const signUpButtonClickHandler = (value) => {
        props.openLoginModal(value);
    };


    return (
        <>
            <nav className={classes['navbar-container']}>
                <div className={classes['logo-wrapper']}>
                    <img src={Logo} className={'img-fluid'} alt={'Site logo'}/>
                </div>
                <ul className={classes['nav-content']}>
                    <li><NavLink to={'/'}>Become a Tutor</NavLink></li>
                    <li><NavLink to={'/'}>Contact Us</NavLink></li>
                    <li><NavLink to={'/'}>About Us</NavLink></li>
                    <li><ButtonRounded onClickHandler={signUpButtonClickHandler}>Sign Up</ButtonRounded></li>
                    <li>
                        <Dropdown>
                            <Dropdown.Toggle variant={'light'}>
                                <FontAwesomeIcon className="mr-2" icon={faGlobe} size={'sm'}/>
                                Language
                            </Dropdown.Toggle>

                            <Dropdown.Menu>
                                <Dropdown.Item href={'/'}>English</Dropdown.Item>
                                <Dropdown.Item href={'/'}>French</Dropdown.Item>
                                <Dropdown.Item href={'/'}>German</Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </li>
                </ul>
            </nav>
        </>
    );
};

export default Navbar;
