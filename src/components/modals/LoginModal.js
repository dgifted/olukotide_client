import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import {Link} from "react-router-dom";
import Form from "react-bootstrap/Form";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFacebookF} from "@fortawesome/free-brands-svg-icons";
import {faGoogle} from "@fortawesome/free-brands-svg-icons";

import classes from './LoginModal.module.css';
import ButtonRounded from "../ButtonRounded";


const LoginModal = (props) => {

    return (
        <Modal {...props} backdrop={'static'} size={'sm'} className={classes['modal-wrapper']}>
            <Modal.Header closeButton className={classes['modal-header']}>
                <h2>Log in</h2>
            </Modal.Header>

            <Modal.Body>
                <div className={`${classes['modal-body-caption']}`}>
                    Don’t have an account? <Button variant={'link'}>Sign up here</Button>
                </div>

                <Form className={'mt-3'}>
                    <Form.Group controlId={'userEmail'}>
                        <Form.Label>Email</Form.Label>
                        <Form.Control type={'email'} style={{padding: '1.5rem 1.0rem !important'}}/>
                    </Form.Group>

                    <Form.Group controlId={'userPassword'}>
                        <Form.Label>Password</Form.Label>
                        <Form.Control type={'password'} />
                    </Form.Group>

                    <ButtonRounded className={`${classes['full-width']} ${classes['button-login']}`}>Get started</ButtonRounded>
                </Form>

                <p className={'text-info text-center my-2'}><Button variant={'link'}>Forgot password?</Button></p>

                <ButtonRounded className={`${classes['full-width']} ${classes['button-facebook-login']}`}>
                    <FontAwesomeIcon icon={faFacebookF} className={'mr-2'}/>
                    <span>Login with Facebook</span>
                </ButtonRounded>

                <ButtonRounded className={`${classes['full-width']} ${classes['button-google-login']} my-3`}>
                    <FontAwesomeIcon icon={faGoogle} className={'mr-2'}/>
                    <span>Login with Google</span>
                </ButtonRounded>
            </Modal.Body>
        </Modal>
    );
};

export default LoginModal;
