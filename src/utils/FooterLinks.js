const footerLinks = [
    { label: 'About us', path: '/about'},
    { label: 'Contact us', path: '/contact'},
    { label: 'Parents', path: '/parents'},
    { label: 'Affiliates', path: '/affiliates'},
    { label: 'Become a tutor', path: '/tutor-enrol'},
    { label: 'Get a tutor', path: '/get-tutor'},
    { label: 'Partner with us', path: '/partner'},
    { label: 'FAQS', path: '/faqs'},
    { label: 'Terms', path: '/terms'},
    { label: 'Privacy policies', path: '/privacy-policies'},
];

export default footerLinks;
