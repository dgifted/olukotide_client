import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFacebookF} from "@fortawesome/free-brands-svg-icons";
import {faGoogle} from "@fortawesome/free-brands-svg-icons";

import classes from './SignUpModal.module.css';
import ButtonRounded from "../ButtonRounded";


const SignUpModal = (props) => {

    return (
        <Modal {...props} backdrop={'static'} size={'sm'} className={classes['modal-wrapper']}>
            <Modal.Header closeButton className={classes['modal-header']}>
                <h2>Sign Up</h2>
            </Modal.Header>

            <Modal.Body>
                <ButtonRounded className={`${classes['full-width']} ${classes['button-facebook-login']}`}>
                    <FontAwesomeIcon icon={faFacebookF} className={'mr-2'}/>
                    <span>Login with Facebook</span>
                </ButtonRounded>

                <ButtonRounded className={`${classes['full-width']} ${classes['button-google-login']} my-3`}>
                    <FontAwesomeIcon icon={faGoogle} className={'mr-2'}/>
                    <span>Login with Google</span>
                </ButtonRounded>

                <div className={classes['separator']}>
                    <hr/>
                    <span className={classes['separator-text']}>OR</span>
                </div>

                <Form className={'mt-3'}>
                    <Form.Group controlId={'userEmail'}>
                        <Form.Label>Email</Form.Label>
                        <Form.Control type={'email'} style={{padding: '1.5rem 1.0rem !important'}}/>
                    </Form.Group>

                    <ButtonRounded
                        className={`${classes['full-width']} ${classes['button-login']}`}>Continue</ButtonRounded>
                </Form>

                <div className={`${classes['modal-body-caption']} text-center mt-2`}>
                    Already have an account?<Button variant={'link'}>Log in</Button>
                </div>

            </Modal.Body>
        </Modal>
    );
};

export default SignUpModal;
