import Logo from '../assets/images/logo-sm.png';
import classes from './Footer.module.css';
import footerLinks from "../utils/FooterLinks";
import {Link} from "react-router-dom";
import Dropdown from "react-bootstrap/Dropdown";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faGlobe} from "@fortawesome/free-solid-svg-icons";

const Footer = () => {
    return (
        <footer className={`container-fluid ${classes['app-footer']}`}>
            <div className={classes['footer-about-section']}>
                <div className={classes['footer-logo-wrapper']}>
                    <img src={Logo} className={'img-fluid'} alt=""/>
                </div>
                <p>Olukotide is a learning exchange platform providing access to limitless resources,
                    teaching and learning across the world. It is your one stop shop for ease and
                    empowerment to our target audience.</p>
            </div>
            <div className={classes['footer-links-section']}>
                <div>
                    {footerLinks.map((link, index) => (
                        <Link to={link.path} key={index} className={classes['footer-link']}>{link.label}</Link>
                    ))}
                </div>
                <div>
                    <Dropdown>
                        <Dropdown.Toggle variant={'light'}>
                            <FontAwesomeIcon className="mr-2" icon={faGlobe} size={'sm'}/>
                            Language
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                            <Dropdown.Item href={'/'}>English</Dropdown.Item>
                            <Dropdown.Item href={'/'}>French</Dropdown.Item>
                            <Dropdown.Item href={'/'}>German</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </div>
            </div>
        </footer>
    );
};

export default Footer;
