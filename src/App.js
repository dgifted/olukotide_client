import {useState} from 'react';
// import {Redirect, NavLink, Route, Link, Switch, useLocation} from 'react-router-dom';
import {Route, Switch, useLocation} from 'react-router-dom';
import './App.css';
import Index from "./pages/Index";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
// import LoginModal from "./components/modals/LoginModal";
import SignUpModal from "./components/modals/SignUpModal";

const App = () => {
    const location = useLocation();
    const [modalShown, setModalShown] = useState(true);

    const openLoginModalHandler = (value) => {
        setModalShown(value);
    };

    return (
        <section>
            <Navbar openLoginModal={openLoginModalHandler}/>
            <Switch location={location} key={location.pathname}>
                <Route path={'/'}>
                    <Index/>
                </Route>
            </Switch>
            <Footer/>

            {/*<LoginModal show={modalShown} onHide={() => setModalShown(false)}/>*/}
            <SignUpModal show={modalShown} onHide={() => setModalShown(false)}/>
        </section>
    );
}

export default App;
