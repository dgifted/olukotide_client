import {useState} from 'react';
import classes from './ButtonRounded.module.css';


const ButtonRounded = (props) => {
    const [modalShown, setModalShown] = useState(false);

    const onClickHandler = () => {
        setModalShown(prevState => {
            return !prevState;
        });

        props.onClickHandler(modalShown);
    }

    return (
        <button className={`${classes['app-btn']} ${props.className}`}
                onClick={onClickHandler}>{props.children}</button>
    );
};

export default ButtonRounded;
